import cv2

class CamSelect:
    def __init__(self):
        self.cam_index = self.device_select()

    def device_select(self):
        index = 0
        cap_ls = []
        while True:
            temp_cap = cv2.VideoCapture(index)
            if not temp_cap.read()[0]:
                break
            else:
                cap_ls.append(index)
                temp_cap.release()
            index += 1
        return cap_ls[-1]