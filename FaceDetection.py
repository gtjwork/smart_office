import os
import cv2
import time
import json
import queue
import ctypes
import pickle
import imutils
import requests
import threading
import numpy as np
import pigpio as GPIO
from queue import Queue
from datetime import datetime
from keras.models import load_model
from argparse import ArgumentParser
from imutils.video import VideoStream
from keras.preprocessing.image import img_to_array

class API:
    def __init__(self, activity, token=None):
        self.request_url = "http://tds-digi-face.gss.com.tw/Digi_Talent_AI_Code/api/Image/" + activity
        self.token = token
    
    def post_identify(self):
        img_form_data = {'file': ('output.jpeg', open('output.jpeg', 'rb'), 'image/jpeg')} 
        if self.token:
            headers = {"Token" : self.token}
            response = requests.post(self.request_url, files=img_form_data, headers=headers)
        else:
            response = requests.post(self.request_url, files=img_form_data)
        print(response.status_code)
        print(response.text)
        return response.json()

class ThreadAPI(threading.Thread):
    def __init__(self, api, user_data_Q, img, flag = False, done = False):
        threading.Thread.__init__(self)
        self.img = img
        self.user_data_Q = user_data_Q
        self.flag = flag
        self.done = done
        self.api = api
    
    def run(self): 
        while True:
            if self.flag is False:
                continue
            cv2.imwrite('output.jpeg', self.img)
            self.user_data_Q.put(self.api.post_identify())
            self.done = True
            self.flag = False
    
    def get_id(self): 
        # returns id of the respective thread 
        if hasattr(self, '_thread_id'): 
            return self._thread_id 
        for id, thread in threading._active.items(): 
            if thread is self: 
                return id

    def raise_exception(self): 
        thread_id = self.get_id() 
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 
              ctypes.py_object(SystemExit)) 
        if res > 1: 
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0) 
            print('Exception raise failure') 

    def check_point(self):
        return self.done

class ThreadGPIO(threading.Thread):
    def __init__(self, flag, delay_time, open_steps, close_steps):
        threading.Thread.__init__(self)
        self.pi = GPIO.pi('192.168.137.135','8888')
        self.pins = [22, 27, 17, 24]
        self.two_phase = ['1001','1100','0110','0011']
        self.flag = False
        self.state = 0
        self.delay_time = delay_time
        self.open_steps = open_steps
        self.close_steps = close_steps
        self.timer_reset = True

    def rotate(self, pins, steps, delay_time):
        for i in range(steps):
            for step in self.two_phase:
                for i in range(len(pins)):
                    self.pi.write(pins[i], step[i] == '1')
                time.sleep(delay_time)
        
    def run(self):
        for pin in self.pins:
            self.pi.set_mode(pin, GPIO.OUTPUT)
        pins, reverse_pins = [24, 17, 27, 22], [22, 27, 17, 24]
        while True:
            if self.flag is False:
                if self.state == 1:
                    print ("Closing door ")
                    self.rotate(reverse_pins, self.close_steps, self.delay_time)
                    self.state = 0
                    print ("Door closed")
                continue
            else:
                if self.state == 0:
                    print ("Opening door")
                    self.rotate(pins, self.open_steps, self.delay_time)
                    time_start = time.time()
                    self.state = 1
                    print ("Door opened")
                time_end = time.time()
                if self.timer_reset is True:
                    time_start = time.time()
                    self.timer_reset = False
                    print ("reset oh~~")
                print ("What's time?", int(time_end - time_start))
                if (time_end - time_start > 8):
                    print ("time's up")
                    self.flag = False

class ThreadGPIO_OpenFile(threading.Thread):
    def __init__(self, flag):
        threading.Thread.__init__(self)
        self.flag = False
        self.state = 0
        self.timer_reset = True
        
    def run(self):
        while True:
            if self.flag is False:
                if self.state == 1:
                    print ("Closing door ")
                    #time.sleep(1)
                    os.system("python MotorControl.py 0")
                    self.state = 0
                    print ("Door closed")
                continue
            else:
                if self.state == 0:
                    #time.sleep(1)
                    print ("Opening door")
                    os.system("python MotorControl.py 1")
                    time_start = time.time()
                    self.state = 1
                    print ("Door opened")
                time_end = time.time()
                if self.timer_reset is True:
                    time_start = time.time()
                    self.timer_reset = False
                    print ("reset oh~~")
                # print ("What's time?", int(time_end - time_start))
                if (time_end - time_start > 8):
                    print ("time's up")
                    self.flag = False

    def get_id(self): 
        # returns id of the respective thread 
        if hasattr(self, '_thread_id'): 
            return self._thread_id 
        for id, thread in threading._active.items(): 
            if thread is self: 
                return id

    def raise_exception(self): 
        thread_id = self.get_id() 
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 
              ctypes.py_object(SystemExit)) 
        if res > 1: 
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0) 
            print('Exception raise failure') 
        
class FaceDetect:
    def __init__(self, api, cam_index):
        self.api = api
        self.login_display = np.zeros((480, 640, 3))
        self.net = cv2.dnn.readNetFromCaffe("deploy.prototxt.txt", "res10_300x300_ssd_iter_140000.caffemodel")
        self.confidence = 0.9
        self.cam_index = cam_index
        self.chosen_img = np.zeros((480,640,3))
        self.night_interface = self.read_night_interface()
        self.morning_interface = self.read_morning_interface()
        self.liveness_model = load_model("liveness.model")
        self.le = pickle.loads(open("le.pickle", "rb").read())

    def read_night_interface(self):
        img = cv2.imread('night_interface.jpg')
        img = cv2.resize(img,(360,480))
        return img

    def read_morning_interface(self):
        img = cv2.imread('morning_interface.jpg')
        img = cv2.resize(img,(360,480))
        return img

    def ui_window(self, now_time):
        hour = int(now_time[11:13])
        if (hour >= 6) and (hour <= 17):
            self.chosen_img = self.morning_interface
        else:
            self.chosen_img = self.night_interface    

    def square_faces(self, detections, image):
        faces = 0
        temp_img = image.copy()
        (h, w) = image.shape[:2]
        for i in range(0, detections.shape[2]):
            # extract the confidence (i.e., probability) associated with the prediction
            confidence = detections[0, 0, i, 2]
            # filter out weak detections by ensuring the `confidence` is
            # greater than the minimum confidence
            if confidence < self.confidence:
                continue
            # compute the (x, y)-coordinates of the bounding box for the object
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            # extract the face ROI and then preproces it in the exact
            # same manner as our training data
            face_startX = max(0, startX)
            face_startY = max(0, startY)
            face_endX = min(w, endX)
            face_endY = min(h, endY)
            face_img = temp_img[face_startY:face_endY, face_startX:face_endX]
            result = self.face_anti_spoofing(face_img)
            if (result["success"] == True):
                # draw the bounding box of the face along with the associated probability
                cv2.rectangle(temp_img, (startX, startY), (endX, endY),(255, 255, 255), 2)
                cv2.putText(temp_img, result["label_txt"], (startX, endY + 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
                # if the num of real face increases 1, then faces += 1
                if (result["label"] == "real"):
                    faces += 1
                # elif (result["label"] == "fake"):
                #     faces = 0
                #     break

            # # draw the bounding box of the face along with the associated probability
            # cv2.rectangle(temp_img, (startX, startY), (endX, endY),(255, 255, 255), 2)
            # faces += 1
        return faces, temp_img

    def face_anti_spoofing(self, face):
        try:
            face = cv2.resize(face, (32, 32))

            face = face.astype("float") / 255.0
            face = img_to_array(face)
            face = np.expand_dims(face, axis=0)

            # pass the face ROI through the trained liveness detector
            # model to determine if the face is "real" or "fake"
            preds = self.liveness_model.predict(face)[0]
            j = np.argmax(preds)
            label = self.le.classes_[j]
            # if (label == "real"):
            #   print("True")

            # draw the label and bounding box on the frame
            label_txt = "{}: {:.2f}".format(label, preds[j])
            result = {  "success": True,
                        "label": label,
                        "confidence": preds[j],
                        "label_txt": label_txt
                     }
        except Exception as e:
            # print(str(e))
            result = {  "success": False,
                        "label": "Wrong Format",
                        "confidence": 0,
                        "label_txt": "None"
                     }
        return result

    def show_data(self,img_message,
              user_info,
              rec_x1, 
              rec_y1, 
              rec_x2, 
              rec_y2, 
              x_name, 
              x_dep, 
              y_name_dep):     
        if (user_info["success"] is False):
            cv2.rectangle(img_message, (rec_x1, rec_y1), (rec_x2, rec_y2), (255,255,255),-1)
            cv2.putText(img_message, user_info["message"], (x_name, y_name_dep), cv2.FONT_HERSHEY_SIMPLEX, 1, (50, 50, 50), 2, cv2.LINE_AA)
        else:
            rec_interval = 65
            for info in user_info["data"]:
                cv2.rectangle(img_message, (rec_x1, rec_y1), (rec_x2, rec_y2), (255,255,255),-1)
                cv2.putText(img_message, info["EName"], (x_name, y_name_dep), cv2.FONT_HERSHEY_SIMPLEX, 1, (50, 50, 50), 2, cv2.LINE_AA)
                cv2.putText(img_message, info["Department"], (x_dep, y_name_dep), cv2.FONT_HERSHEY_SIMPLEX, 1, (150, 150, 150), 2, cv2.LINE_AA)
                y_name_dep += rec_interval
                rec_y1 += rec_interval
                rec_y2 += rec_interval

    def login_detecting(self):
        net = self.net
        cap = cv2.VideoCapture(self.cam_index)
        shutdown_cam = False
        failed = True
        faces, previous_face_num = 0, 0
        user_data = []
        activity_list = []
        result_txt = ""
        result_dep = ""
        token = ""
        user_data_Q, APIimgQ = Queue(), Queue()
        flag = False
        test_img = np.zeros((480,640,3))
        time_start = time.time()
        color = (0,0,0)
        
        threadAPI = ThreadAPI(self.api, user_data_Q, test_img, flag)
        threadAPI.setDaemon(True)
        threadAPI.start()

        while (shutdown_cam is False) and (cap != None):
            time_end = time.time()
            # grab the frame from the threaded video stream and resize it to have a maximum width of 400 pixels
            ret, img = cap.read()

            # grab the frame dimensions and convert it to a blob
            (h, w) = img.shape[:2]
            blob = cv2.dnn.blobFromImage(cv2.resize(img, (300, 300)), 1.0,(300, 300), (104.0, 177.0, 123.0)) 

            # pass the blob through the network and obtain the detections and predictions
            net.setInput(blob)
            detections = net.forward()

            faces, tmp_img = self.square_faces(detections, img)
            #overlay_img = img.copy()

            if ((faces-previous_face_num) >= 1):
                APIimgQ.put(img)

            if (APIimgQ.qsize() >= 1):
                threadAPI.img = APIimgQ.get()
                threadAPI.flag = True
                time_start = time.time()
                print ("API") 

            if (user_data_Q.qsize() >= 1):
                user_data = user_data_Q.get()

            if len(user_data) != 0:
                if user_data["success"] == True:
                    
                    failed = False
                    datas = user_data["data"]
                    # user_info: the 1st item in the datas
                    user_info = datas["Item1"]
                    #result_dep = user_info[0]["Department"]
                    shutdown_cam = True
                    activity_list = datas["Item2"]
                    token = user_info[0]["Token"]
                    if activity_list != []:
                        color = (0, 255, 0)
                        result_txt = user_info[0]["EName"]
                    else:
                        color = (0, 97, 255)
                        result_txt = "Unauthorized"
                elif user_data["message"] == "Detect Failed":
                    failed = True
                    color = (0, 97, 255)
                    result_txt = user_data["message"]
                    # Retry again
                    faces = 0
                elif user_data["message"] == "Identify Failed":
                    failed = True
                    color = (0, 97, 255)
                    result_txt = user_data["message"]
                else:
                    result_txt = user_data["message"]
                    # Retry again
                    faces = 0
                
            user_data = []
            if result_txt != "":
                cv2.putText(tmp_img, result_txt, (260, 60), cv2.FONT_HERSHEY_SIMPLEX, 1.5, color, 3, cv2.LINE_AA)
                cv2.rectangle(tmp_img, (0,0), (640,480), color, 30)
            
            cv2.putText(tmp_img, "Press esc to exit", (15, 35), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 97, 255), 2, cv2.LINE_AA)
            cv2.moveWindow("Login FaceEntrance", 600, 200)
            cv2.imshow("Login FaceEntrance", tmp_img)
            
            previous_face_num = faces
            # if the `esc` key was pressed, break from the loop
            if cv2.waitKey(1) & 0xFF == 27:
                break
        # do a bit of cleanup
        time.sleep(2)
        cap.release()
        cv2.destroyAllWindows()

        threadAPI.raise_exception() 
        #threadAPI.join() 
        return activity_list, token

    def onWork_detecting(self):
        net = self.net
        cap = cv2.VideoCapture(self.cam_index)

        faces, previous_face_num = 0, 0
        user_data = []
        user_data_Q, APIimgQ = Queue(), Queue()
        time_start = time.time()
        flag = False
        test_img = np.zeros((480,640,3))

        threadGPIO = ThreadGPIO(flag, delay_time=0.000002, close_steps=54, open_steps=54)
        threadGPIO.setDaemon(True)
        threadGPIO.start()

        threadAPI = ThreadAPI(self.api, user_data_Q, test_img, flag)
        threadAPI.setDaemon(True)
        threadAPI.start()

        # setup background's style
        now_time = str(datetime.now().strftime("%Y-%m-%d %H:%M"))
        # setup background's style
        img_UI = self.ui_window(now_time)
        blur_img_UI = self.chosen_img

        while (cap != None):
            time_end = time.time()
            
            # grab the frame from the threaded video stream and resize it to have a maximum width of 400 pixels
            ret, img = cap.read()

            # grab the frame dimensions and convert it to a blob
            (h, w) = img.shape[:2]
            blob = cv2.dnn.blobFromImage(cv2.resize(img, (300, 300)), 1.0,(300, 300), (104.0, 177.0, 123.0)) 

            # pass the blob through the network and obtain the detections and predictions
            net.setInput(blob)
            detections = net.forward()

            faces, tmp_img = self.square_faces(detections, img)

            if ((faces-previous_face_num) >= 1):
                APIimgQ.put(img)

            if (APIimgQ.qsize() >= 1):
                threadAPI.img = APIimgQ.get()
                threadAPI.flag = True
                print ("API")
            
            # UI window
            self.ui_window(now_time)  

            if (user_data_Q.qsize() >= 1):
                user_data = user_data_Q.get()
                time_start = time.time()
                
            if ((time_end - time_start) >= 5):
                blur_img_UI = self.chosen_img
            
            # copy a "img_UI" to make blur boxes with user_data
            overlay_message = self.chosen_img.copy()

            if (len(user_data) != 0): 
                # show data on screen
                if (user_data["success"] == True):
                    threadGPIO.flag = True
                    threadGPIO.timer_reset = True

                self.show_data(overlay_message,
                        user_data,
                        rec_x1 = 20,
                        rec_y1 = 130,
                        rec_x2 = 330,
                        rec_y2 = 170,
                        x_name = 35,
                        x_dep = 240,
                        y_name_dep = 160
                        )

                if user_data["message"] == "Detect Failed":
                    faces = 0
                user_data = []
                blur_img_UI = cv2.addWeighted(overlay_message, 0.8, self.chosen_img, 1 - 0.8, 0)
                imgs = np.hstack([tmp_img, blur_img_UI])
                
            previous_face_num = faces

            imgs = np.hstack([tmp_img, blur_img_UI])

            # show the output frame
            cv2.namedWindow("FaceDetection", cv2.WINDOW_NORMAL)
            cv2.setWindowProperty("FaceDetection", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
            cv2.imshow("FaceDetection", imgs)
            
            # if the `esc` key was pressed, break from the loop
            if cv2.waitKey(1) & 0xFF == 27:
                break

        # do a bit of cleanup
        cap.release()
        cv2.destroyAllWindows()
        threadAPI.raise_exception() 
        # threadAPI.join() 
        # to make door closed
        time.sleep(30)
        print ("[INFO] Program finished.")

    def onWork_detecting_openFile(self):
        net = self.net
        cap = cv2.VideoCapture(self.cam_index)

        faces, previous_face_num = 0, 0
        user_data = []
        user_data_Q, APIimgQ = Queue(), Queue()
        time_start = time.time()
        flag = False
        test_img = np.zeros((480,640,3))

        threadGPIO_openFile = ThreadGPIO_OpenFile(flag)
        threadGPIO_openFile.setDaemon(True)
        threadGPIO_openFile.start()

        threadAPI = ThreadAPI(self.api, user_data_Q, test_img, flag)
        threadAPI.setDaemon(True)
        threadAPI.start()

        # setup background's style
        now_time = str(datetime.now().strftime("%Y-%m-%d %H:%M"))
        self.ui_window(now_time)
        blur_img_UI = self.chosen_img

        while (cap != None):
            now_time = str(datetime.now().strftime("%Y-%m-%d %H:%M"))
            time_end = time.time()
            
            # grab the frame from the threaded video stream and resize it to have a maximum width of 400 pixels
            ret, img = cap.read()

            # grab the frame dimensions and convert it to a blob
            (h, w) = img.shape[:2]
            blob = cv2.dnn.blobFromImage(cv2.resize(img, (300, 300)), 1.0,(300, 300), (104.0, 177.0, 123.0)) 

            # pass the blob through the network and obtain the detections and predictions
            net.setInput(blob)
            detections = net.forward()

            faces, tmp_img = self.square_faces(detections, img)

            if ((faces-previous_face_num) >= 1):
                APIimgQ.put(img)

            if (APIimgQ.qsize() >= 1):
                threadAPI.img = APIimgQ.get()
                threadAPI.flag = True
                print ("API")
            
            # UI window
            self.ui_window(now_time)  

            if (user_data_Q.qsize() >= 1):
                user_data = user_data_Q.get()
                time_start = time.time()
                
            if ((time_end - time_start) >= 5):
                blur_img_UI = self.chosen_img
            
            # copy a "img_UI" to make blur boxes with user_data
            overlay_message = self.chosen_img.copy()

            if (len(user_data) != 0): 
                # show data on screen
                if (user_data["success"] == True):
                    threadGPIO_openFile.flag = True
                    threadGPIO_openFile.timer_reset = True

                self.show_data(overlay_message,
                        user_data,
                        rec_x1 = 20,
                        rec_y1 = 130,
                        rec_x2 = 330,
                        rec_y2 = 170,
                        x_name = 35,
                        x_dep = 240,
                        y_name_dep = 160
                        )

                if user_data["message"] == "Detect Failed":
                    faces = 0
                user_data = []
                blur_img_UI = cv2.addWeighted(overlay_message, 0.8, self.chosen_img, 1 - 0.8, 0)
                imgs = np.hstack([tmp_img, blur_img_UI])
                
            previous_face_num = faces
            cv2.putText(tmp_img, now_time, (15, 35), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255), 2, cv2.LINE_AA) #Show up datetime
            cv2.putText(tmp_img, "Press esc to exit", (15, 470), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255), 2, cv2.LINE_AA)
            imgs = np.hstack([tmp_img, blur_img_UI])

            # show the output frame
            cv2.namedWindow("FaceDetection", cv2.WINDOW_NORMAL)
            cv2.setWindowProperty("FaceDetection", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
            cv2.imshow("FaceDetection", imgs)
            
            # if the `esc` key was pressed, break from the loop
            if cv2.waitKey(1) & 0xFF == 27:
                break

        # do a bit of cleanup
        cap.release()
        cv2.destroyAllWindows()

        time.sleep(13)
        threadAPI.raise_exception()
        threadGPIO_openFile.raise_exception() 
        # threadGPIO_openFile.join() 
        # to make door closed
        print ("finish program")
   
    def activity_detecting(self):
        net = self.net
        cap = cv2.VideoCapture(self.cam_index)

        faces, previous_face_num = 0, 0
        user_data = []
        user_data_Q, APIimgQ = Queue(), Queue()
        time_start = time.time()
        flag = False
        test_img = np.zeros((480,640,3))

        threadAPI = ThreadAPI(self.api, user_data_Q, test_img, flag)
        threadAPI.setDaemon(True)
        threadAPI.start()

        now_time = str(datetime.now().strftime("%Y-%m-%d %H:%M"))
        # setup background's style
        img_UI = self.ui_window(now_time)
        blur_img_UI = self.chosen_img

        while (cap != None):
            now_time = str(datetime.now().strftime("%Y-%m-%d %H:%M"))
            time_end = time.time()
            
            # grab the frame from the threaded video stream and resize it to have a maximum width of 400 pixels
            ret, img = cap.read()

            # grab the frame dimensions and convert it to a blob
            (h, w) = img.shape[:2]
            blob = cv2.dnn.blobFromImage(cv2.resize(img, (300, 300)), 1.0,(300, 300), (104.0, 177.0, 123.0)) 

            # pass the blob through the network and obtain the detections and predictions
            net.setInput(blob)
            detections = net.forward()

            faces, tmp_img = self.square_faces(detections, img)

            if ((faces-previous_face_num) >= 1):
                APIimgQ.put(img)

            if (APIimgQ.qsize() >= 1):
                threadAPI.img = APIimgQ.get()
                threadAPI.flag = True
                print ("API")
            
            # UI window
            self.ui_window(now_time)

            if (user_data_Q.qsize() >= 1):
                user_data = user_data_Q.get()
                time_start = time.time()
                
            if ((time_end - time_start) >= 5):
                blur_img_UI =  self.chosen_img #img_UI
            
            # copy a "img_UI" to make blur boxes with user_data
            overlay_message = self.chosen_img.copy()
            if (len(user_data) != 0): 
                # show data on screen

                self.show_data(overlay_message,
                        user_data,
                        rec_x1 = 20,
                        rec_y1 = 130,
                        rec_x2 = 330,
                        rec_y2 = 170,
                        x_name = 35,
                        x_dep = 240,
                        y_name_dep = 160
                        )
                
                if user_data["message"] == "Detect Failed":
                    faces = 0
                user_data = []
                blur_img_UI = cv2.addWeighted(overlay_message, 0.8, self.chosen_img, 1 - 0.8, 0)
                imgs = np.hstack([tmp_img, blur_img_UI])
            
            previous_face_num = faces
            cv2.putText(tmp_img, now_time, (15, 35), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255), 2, cv2.LINE_AA) #Show up datetime
            cv2.putText(tmp_img, "Press esc to exit", (15, 470), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255), 2, cv2.LINE_AA)
            imgs = np.hstack([tmp_img, blur_img_UI])

            # show the output frame
            cv2.namedWindow("FaceDetection", cv2.WINDOW_NORMAL)
            cv2.setWindowProperty("FaceDetection", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
            cv2.imshow("FaceDetection", imgs)#imgs
            
            # if the `esc` key was pressed, break from the loop
            if cv2.waitKey(1) & 0xFF == 27:
                break
        
        # do a bit of cleanup
        cap.release()
        cv2.destroyAllWindows()

        threadAPI.raise_exception() 
        #threadAPI.join() 

if __name__ in "__main__":
    
    from CamSelect import CamSelect
    token = "813e103895494cd99529a6af3527c2e5"
    api = API("97", token)
    cam = CamSelect()
    FD_login = FaceDetect(api, cam.cam_index)
    FD_login.onWork_detecting_openFile()

    '''
    parser = ArgumentParser()
    parser.add_argument("cam", type = int)
    parser.add_argument("-t", "--token", dest="token")
    parser.add_argument("-a", "--activity-id", dest="activity_id")
    args = parser.parse_args()

    api = API(args.activity_id, args.token)
    start_detect = FaceDetect(api, args.cam)
    if args.activity_id == "3":
        start_detect.onWork_detecting()
    else:
        start_detect.activity_detecting()
    '''
