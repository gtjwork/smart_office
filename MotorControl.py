import time
import pigpio as GPIO
from argparse import ArgumentParser

def rotate(pins, steps, delay_time):
    for i in range(steps):
        for step in two_phase:
            for i in range(len(pins)):
                pi.write(pins[i], step[i] == '1')
            time.sleep(delay_time)
        
if __name__ in "__main__":
    pi = GPIO.pi('192.168.137.97','8888')
    pins = [22, 27, 23, 24]

    for pin in pins:
        pi.set_mode(pin, GPIO.OUTPUT)

    #one_phase = ['1000','0100','0010','0001']
    two_phase = ['1001','1100','0110','0011']
    #one-two_phase = ['1000','1100','0100','0110','0010','0011','0001','1001']

    pins = [24, 23, 27, 22]
    reverse_pins = pins
    delay_time = 0.002
    open_steps = 54
    close_steps = 54

    parser = ArgumentParser()
    parser.add_argument("turn", type = int)
    args = parser.parse_args()

    if args.turn == 1:
        rotate(pins, open_steps, delay_time)
    else:
        rotate(reverse_pins, close_steps, delay_time)