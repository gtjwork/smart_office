<img src="https://i.imgur.com/OgRUipi.png" width="500" height=150/>

##### Author: Jeff Liao
##### Last updated: 12/19/2019
---
FaceEntrance is a system combined with punching time card and checking in activities by face recognition.
  - Authentication to start the application
  - Punch time card and check in

## Pre-requisites
 1) Install virtual envronmeant -  [Anaconda](https://www.anaconda.com/distribution/)
 2) Install computation acceleration suite -  [CUDA](https://developer.nvidia.com/cuda-downloads) & [cuDNN](https://developer.nvidia.com/cudnn)

## Set up
### 1. Create and Enter virtual environment
```
C:\> conda create -n antispoofing pip python=3
C:\> activate antispoofing
```

### 2. Install Python packages
```
(antispoofing)C:\> pip install PyQt5
(antispoofing)C:\> pip install opencv-python
(antispoofing)C:\> pip install requests
(antispoofing)C:\> pip install pigpio
(antispoofing)C:\> pip install tensorflow-gpu
(antispoofing)C:\> pip install Keras
(antispoofing)C:\> pip install imutils
```
If there is no GPU in your device, you need to use `pip install tensorflow` running code on CPU instead.

### 3. Install [IP scanner](https://angryip.org/) & [VNC Viewer](https://www.realvnc.com/en/connect/download/viewer/)
#### IP scanner
(1) Set up Network sharing 
Settings > Network & Internet > Change Adapter options > Wi-Fi(Properties) > Sharing > Choose Enthernet enabled

(2) Find `ethernet address` connected with Raspberrypi to control stepper motor. 
```
C:\> ipconfig
```
1)Use IP scanner scan raspberrypi and get its ip address
2)Modify ip address in [MotorControl.py](https://git.gss.com.tw/digi-talent_ai/digi_talent_intelligentoffice_code/blob/master/MotorControl.py) and check every pin number is right.

#### VNC Viewer
Shutdown Raspberrypi in a proper way:
1) Enter ip address we got and log in Raspberrypi via VNC Viewer
2) Shutdown it

### 4. Let's FaceEntrance ! ! ! !
```
(antispoofing)C:\> python FaceEntrance.py
```
#### (1) [Start] Click the `START` 
<img src="https://i.imgur.com/wJslEhY.png" width="240" height=400/>

#### (2) [Authentication] Face Recognition
* Successful - You are a manager and get activity list
<img src="https://i.imgur.com/roPRujc.png" width="284" height=218/>

* Failed - You're not a manager and cannot use the service
<img src="https://i.imgur.com/BloQVfE.png" width="284" height=218/>

#### (3) [Activity list] Select an activity and click `START`
<img src="https://i.imgur.com/sUp8oRI.png" width="240" height=400/>

If you click `Logout`, it will return home page.
#### (4) [Check in] Face Recognition

Once you're recognized as participants in the activity, the results will be show up on screen.

<img src="https://i.imgur.com/yNdn2Q4.png" width="600" height=400/>

PS. Choose the activity of punching time card, we can control motor with raspberrypi to open a door.