import os
import sys
import FaceDetection as FD
from PyQt5 import QtCore, QtGui
from CamSelect import CamSelect
from PyQt5.QtWidgets import QMainWindow, QWidget, QApplication, QPushButton, QListWidget, QVBoxLayout, QLabel

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.init_ui()
        self.activity_list = []
    
    def init_ui(self):
        self.setWindowIcon(QtGui.QIcon("GSSXDIGI_Logo.png"))
        self.MainWindow_stylesheet = """
        QMainWindow{
            background-image:url("login_interface.jpg")
        }
        """
        self.setWindowTitle("Welcome to FaceEntrance")
        self.setGeometry(700,100,450,850)
        self.setFixedSize(450,850)
        self.setStyleSheet(self.MainWindow_stylesheet)

        self.startBtn_stylesheet = """
        QPushButton{
            background-color: #4CBDCC;
            border-style: outset;
            border-width: 6px;
            border-radius: 22px;
            border-color: beige;
            font: bold 20px ;
            color: #FFFFFF;
            font-family: Microsoft JhengHei;
            min-width: 10em;
            padding: 10px;
        }
        """
        self.startBtn = QPushButton("START",self)
        self.startBtn.setEnabled(True)
        self.startBtn.setGeometry(90, 648, 255, 80)
        self.startBtn.setStyleSheet(self.startBtn_stylesheet)
        self.startBtn.clicked.connect(self.login)

    def login(self):
        self.startBtn.setEnabled(False)
        self.activity_list = []
        api_login = FD.API("Manager")
        FD_login = FD.FaceDetect(api_login, cam.cam_index)
        self.activity_list, token = FD_login.login_detecting()
        self.startBtn.setEnabled(True)
        if (len(self.activity_list) != 0): 
            self.hide()
            self.activityWindow = ActivityWindow(token)
            self.activityWindow.show()

class ActivityWindow(QMainWindow):
    def __init__(self, token):
        super(ActivityWindow, self).__init__()
        self.init_ui()
        self.token = token
        self.activity_id = -1

    def init_ui(self):
        ActivityWindow_stylesheet = """
        QMainWindow{
            background-image:url("activity_interface.jpg")
        }
        """
        self.setWindowTitle("Welcome to FaceEntrance")
        self.setWindowIcon(QtGui.QIcon("GSSXDIGI_Logo.png"))
        self.setStyleSheet(ActivityWindow_stylesheet)
        self.setGeometry(700,100,450,850)
        self.setFixedSize(450,850)

        self.noticeLabel_stylesheet = """
        QLabel{
            font: bold 18px;
            font-family: Microsoft JhengHei;
            color: #FF3333;
        }
        """
        self.noticeLabel = QLabel("Please Select an Activity to Start",self)
        self.noticeLabel.setGeometry(82, 788, 300, 80)
        self.noticeLabel.setObjectName("NoticeLabel")
        self.noticeLabel.setStyleSheet(self.noticeLabel_stylesheet)
        self.noticeLabel.hide()

        self.logoutBtn_stylesheet = """
        QPushButton{
            background-color: #FFFFFF;
            border-style: outset;
            border-width: 10px;
            border-color: #FFFFFF;
            font: bold 18px ;
            color: #4CBDCC;
        }
        """
        self.logoutBtn = QPushButton("Logout",self)
        self.logoutBtn.setGeometry(183, 780, 80, 40)
        self.logoutBtn.setStyleSheet(self.logoutBtn_stylesheet)
        self.logoutBtn.clicked.connect(self.back_mainWin)

        self.startBtn_stylesheet = """
        QPushButton{
            background-color: #FFFFFF;
            border-style: outset;
            border-width: 6px;
            border-radius: 22px;
            border-color: #4CBDCC;
            font: bold 20px ;
            color: #4CBDCC;
            font-family: Microsoft JhengHei;
            min-width: 10em;
            padding: 5px;
        }
        """
        self.startBtn = QPushButton("START",self)
        self.startBtn.setGeometry(87, 705, 100, 80)
        self.startBtn.setStyleSheet(self.startBtn_stylesheet)
        self.startBtn.clicked.connect(self.start_activity)
       
        self.listWidget_stylesheet = """
        QListWidget{
            border-style: outset;
            border-width: 5px;
            border-radius: 20px;
            border-color: #FFFFFF;
            font: bold 20px ; 
            font-family: Microsoft JhengHei;
            color: #4CBDCC;
        }
        QListWidget::Item{
            padding: 40px 0px 0px 0px; 
        }
        """
        self.listWidget = QListWidget(self)
        self.listWidget.setGeometry(QtCore.QRect(35,50,379,634))
        self.listWidget.setObjectName("ActivityList")
        self.listWidget.setStyleSheet(self.listWidget_stylesheet)
        print (window.activity_list)
        for activity in window.activity_list:
            self.listWidget.addItem(activity["ActivityName"])
        self.listWidget.itemClicked.connect(self.select_activity)

    def back_mainWin(self):
        window.show()
        self.close()

    def select_activity(self):
        self.activity_id = window.activity_list[self.listWidget.currentRow()]["ActivityId"]
        self.noticeLabel.hide()

    def start_activity(self):
        print (self.activity_id)
        if self.activity_id != -1:
            api_start_activity = FD.API(str(self.activity_id), self.token)
            FD_start_activity = FD.FaceDetect(api_start_activity, cam.cam_index)
            self.hide()
            if self.activity_id == 2120:
                FD_start_activity.onWork_detecting_openFile() 
            else:
                FD_start_activity.activity_detecting() 
            window.show()
        else:
            self.noticeLabel.show()

if __name__ == "__main__":
    cam = CamSelect()
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())